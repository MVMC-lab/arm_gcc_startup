/**
 * @file rtc.c
 * @author LCY
 * @brief LPC1769 RTC 相關基礎功能實作
 * @date 2020.04.17
 */
#include "rtc.h"

void rtc_init(uint16_t year, uint8_t month, uint8_t day_of_month, uint8_t hour, uint8_t min, uint8_t sec) {
    // RTC power setting
    PCONP |= PCONP_RTC;
    
    // Clock Control Register
    // Bit 0 : The time counters are enabled.
    // Bit 1 : elements in the internal oscillator divider are reset.
    // Bit 4 : The calibration counter is enabled and counting.
    RTC_CCR = 0;

    // Time Counter Group:
    // can be read / write
    RTC_SEC     = sec;
    RTC_MIN     = min;
    RTC_HOUR    = hour;
    RTC_DOM     = day_of_month;
    RTC_MONTH   = month;
    RTC_YEAR    = year;
}

void rtc_alarm_init(uint16_t year, uint8_t month, uint8_t day_of_month, uint8_t hour, uint8_t min, uint8_t sec) {
    // Alarm register group
    RTC_ALSEC  = sec;
    RTC_ALMIN  = min;
    RTC_ALHOUR = hour;
    RTC_ALDOM  = day_of_month;
    RTC_ALMON  = month;
    RTC_ALYEAR = year;
}

void rtc_start(void) {
    // Clock Control Register
    // Bit 0 : The time counters are enabled.
    // Bit 1 : elements in the internal oscillator divider are reset.
    // Bit 4 : The calibration counter is enabled and counting.
    RTC_CCR = 0b1;
}

void rtc_stop(void) {
    RTC_CCR = 0;
}

void rtc_current_time(void) {
    // Consolidated time registers
    // Time read registers:
    // Time Counters can optionally be read in a consolidated format
    // RTC_CTIME0
    // Bit 5:0    : Seconds     (0 ~ 59)
    // Bit 13:8   : Minutes     (0 ~ 59)
    // Bit 20:16  : Hours       (0 ~ 23)
    // Bit 26:24  : Day Of Week (0 ~ 6)
    // RTC_CTIME1
    // Bit 4:0    : Day of Month    (1 ~ month days)
    // Bit 11:8   : Month           (1 ~ 12)
    // Bit 27:16  : Year            (0 ~ 4095)
    // RTC_CTIME2
    // Bit 11:0   : Day of Year (1 ~ 365)
    time_str current_time;
    current_time.sec          = (RTC_CTIME0 & 0x3F);
    current_time.min          = (RTC_CTIME0 & 0x3F00) >> 8;
    current_time.hour         = (RTC_CTIME0 & 0x1F0000) >> 16;
    current_time.day_of_month = (RTC_CTIME1 & 0x1F);
    current_time.month        = (RTC_CTIME1 & 0xF00) >> 8;
    current_time.year         = (RTC_CTIME1 & 0xFFF0000) >> 16;

    printf("%4d", current_time.year);
    printf(".%2d", current_time.month);
    printf(".%2d", current_time.day_of_month);
    printf("    %2d", current_time.hour);
    printf(":%2d", current_time.min);
    printf(":%2d\n", current_time.sec);
}

void rtc_irq_en(void) {
    // Counter Increment Interrupt Register
    // Control which generates an interrupt when set to 1
    // Bit 0 : Second value
    // Bit 1 : Minute
    // Bit 2 : Hour
    // Bit 3 : Day of Month
    // Bit 4 : Day of Week
    // Bit 5 : Day of Year
    // Bit 6 : Month
    // Bit 7 : Year
    RTC_CIIR = 0b1;

    // Alarm Mask Register
    // Control which generates an interrupt when set to 1
    // Bit 0 : Second value
    // Bit 1 : Minute
    // Bit 2 : Hour
    // Bit 3 : Day of Month
    // Bit 4 : Day of Week
    // Bit 5 : Day of Year
    // Bit 6 : Month
    // Bit 7 : Year
    RTC_AMR = 0b0;
}

void rtc_irq_disable(void) {
    RTC_ILR  = 0;
    RTC_CIIR = 0;
    RTC_AMR  = 0;
}