/**
 * @file system.c
 * @author LiYu87
 * @date 2020.01.07
 * @brief system clock、system power、pinconnect之設定函式。
 */

#include "system.h"

#include "LPC1769.h"

#include "regdef.h"

/**
 * CONFIG
 */
#define PLL0_M 20
#define PLL0_N 1
#define CPU_CLK_DEVIDER 4

/**
 * clk_pll0 = (2 *  M * sys_clk) / N
 *          = (2 * 20 *    12e6) / 1
 *          = 480e6
 *          = 480 MHz
 *
 * M should be 6~512
 * N should be 1~32
 *
 * clk_cpu = clk_pll0 / Devider
 *         = 480 MHz  / 4
 *         = 120 MHz
 */
void system_clock_init(void) {
    // enable main oscillator
    SCS = SCS_OSCEN;

    // select osc_clk as sys_clk
    CLKSRCSEL = CLKSRCSEL_CLKSRC_OSC;

    // Wait for Oscillator to be ready
    while ((SCS & SCS_OSCSTAT) == 0) {
        ;
    }

    // set PLL0 multiplier M and divider N
    PLL0CFG =
        ((PLL0_M - 1) << PLL0CFG_MSEL_POS) | ((PLL0_N - 1) << PLL0CFG_NSEL_POS);
    PLL0FEED = 0xAA;
    PLL0FEED = 0x55;

    // enable and connect PLL0
    PLL0CON = PLL0CON_ENABLE | PLL0CON_CONNECT;
    PLL0FEED = 0xAA;
    PLL0FEED = 0x55;

    // Wait for PLL0 locked
    while (!(PLL0STAT & PLL0STAT_LOCK)) {
        ;
    }

    // Wait for PLL0 enabled and connected
    while ((PLL0STAT & (PLL0STAT_ENABLE | PLL0STAT_CONNECT)) !=
           (PLL0STAT_ENABLE | PLL0STAT_CONNECT)) {
        ;
    }

    CCLKCFG = CPU_CLK_DEVIDER - 1;
}

void system_clock_deinit(void) {
    PLL0CON = 0;
    PLL0FEED = 0xAA;
    PLL0FEED = 0x55;

    PLL0CFG = 0;
    PLL0FEED = 0xAA;
    PLL0FEED = 0x55;

    SCS = 0;
    CLKSRCSEL = 0;

    CCLKCFG = 0;
    PCLKSEL0 = 0;
    PCLKSEL1 = 0;
}

void system_power_init(void) {
    PCONP = PCONP_UART3 | PCONP_GPIO;
}

void system_power_deinit(void) {
    PCONP = 0;
}

void pinconnect_init(void) {
    // set P0.0 and P0.1 as TXD3 and RXD3
    PINSEL0 |= (PIN_FUNC_2 << 0) | (PIN_FUNC_2 << 2);
}

void pinconnect_deinit(void) {
    PINSEL0 = 0;
}
