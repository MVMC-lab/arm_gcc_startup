/**
 * @file ssp.h
 * @author LCY
 * @brief SSP 相關函式定義
 * @date 2020.05.04
 * 
 * Notes: 本函式目前僅有 SSP0，待日後需求可再新增其他 SSP
 */

#ifndef SSP_H
#define SSP_H

#include "system.h"
#include "LPC1769.h"
#include "regdef.h"

/**
 * @brief SSP0 寫入函式
 * 
 * @param tx 寫入資料指標
 * @param len 寫入資料大小 (bytes)
 */
void ssp0_write(uint8_t *tx, uint16_t len);

/**
 * @brief SSP0 讀取函式
 * 
 * @param rx 寫入資料指標
 * @param len 寫入資料大小 (bytes)
 */
void ssp0_read(uint8_t *rx, uint16_t len);

void ssp1_write(uint8_t *tx, uint16_t len);

void ssp1_read(uint8_t *rx, uint16_t len);

#endif // SSP_H