/**
 * @file eeprom.h
 * @author LCY
 * @brief EEPROM 相關函式定義
 * @date 2020-04-24
 * 
 * 硬體相關 :
 *      (1) 介面       : SSP0 (P1.20, P1.23, P1.24)
 *      (2) 記憶體大小 : 0x0000 ~ 0xFFFF
 *      (3) CS 腳位    : P3.25 (P0.24) 
 *          由於 EEPROM CS使用腳位並未拉出，方便 debug ，
 *          使用另一隻 IO 與 EEPROM IO 同時做動，使使用者能以邏輯分析儀觀察
 * 注意點 :
 *      (1) 寫入保護 :
 *          欲寫入時須先解除保護，在本寫入函式亦有實作
 *      (2) 操作間隔 :
 *          每次讀或寫之後，如欲繼續進行動作，需有至少 5ms 的時間間隔
 *          ** 以下函式並沒有處理操作間隔時間，使用者使用時須自行注意 **
 *          (from 規格書，血淚踩雷)
 *      (3) 單次寫入數量限制 :
 *          每次寫入數量最多為 32 bytes
 */

#ifndef EEPROM_H
#define EEPROM_H

#include "system.h"
#include "LPC1769.h"
#include "regdef.h"
#include "ssp.h"

/**
 * @brief EEPROM 初始化函式 
 * 
 */
void eep_init(void);

/**
 * @brief EEPROM 資料寫入函式
 * 
 * @param write_data 寫入資料指標
 * @param addr 欲寫入記憶體之位置 (0x0000 ~ 0xFFFF)
 * @param len 寫入資料 bytes 數量 (1 ~ 32)
 */
void eep_write(uint8_t *write_data, uint16_t addr, uint16_t len);

/**
 * @brief 
 * 
 * @param read_data 讀取資料指標
 * @param addr 欲讀取記憶體之位置 (0x0000 ~ 0xFFFF)
 * @param len 讀取資料 bytes 數量
 */
void eep_read(uint8_t *read_data, uint16_t addr, uint16_t len);

/**
 * @brief SSP0 CS 腳位設定函式，於 eep_init 中使用，無須使用此函式
 * 
 */
void eep_cs_pinset(void);

/**
 * @brief SSP0 CS 腳位致能函式，無須使用此函式
 * 
 */
void eep_cs_enable(void);

/**
 * @brief SSP0 CS 腳位禁能函式，無須使用此函式
 * 
 */
void eep_cs_disable(void);

#endif // EEPROM_H