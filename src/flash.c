/**
 * @file flash.c
 * @author LCY
 * @brief FLASH 相關函式實作
 * @date 2020.04.28
 */

#include "flash.h"

// CS pin for logic analyzer
#define P0TMPCS 25

// write protection define
#define WRITE_ENABLE    0x06
#define WRITE_DISABLE   0x04

// read define
#define READ_DATA       0x03
#define FAST_READ       0x0B
#define PAGE_PROGRAM    0x02

// chip erase define
#define SECTOR_ERASE_4K 0x20
#define BLOCK_ERASE_32K 0x52
#define BLOCK_ERASE_64K 0xD8
#define CHIP_ERASE      0xC7

// device information
#define READ_MANUFAC    0x90
#define MANUFAC_ID_LEN  0x02

uint8_t instr;

void flash_init(void){
    flash_cs_pinset();
    // SSP0 power setting
    PCONP |= PCONP_SSP0;

    // SSP0 clock setting
    // PCLK_peripheral = CCLK/4
    PCLKSEL1 &= ~(0xC00);

    // SSP0 pin setting
    // MOSI : P1.24
    // MISO : P1.23
    // SCK  : P1.20
    // CS   : P3.25
    // (1) Set function to be SSP0.
    // (2) Pin has neither pull-up nor pull-down resistor enabled.
    PINSEL3 |= 0x3C300;
    PINMODE3 |= 0x28200;
    // (1) Set function to be GPIO for SSP0.
    // (2) Pin has an on-chip pull-up resistor enabled.
    PINSEL1 &= ~(0xC00);
    PINMODE7 &= ~(0xC00);

    // basic operation of the SSP controller.
    // Data Size Select : 8-bit transfer
    SSP0CR0 = 0b000111;

    // SSP0 Enable.
    SSP0CR1 = 0b10;

    // Software can write data to be transmitted to this register, and read data that has been received
    SSP0ICR = 3;

    // Prescaler divides the SSP peripheral clock SSP_PCLK
    SSP0CPSR = 30;
}

void flash_write(uint8_t *write_data, uint64_t addr, uint16_t len){
    uint8_t addr_8;

    flash_cs_enable();

    // 1 Byte write enable
    instr = WRITE_ENABLE;
    ssp0_write(&instr, 1);

    flash_cs_disable();
    flash_cs_enable();

    // 1 Byte write instrustion
    instr = PAGE_PROGRAM;
    ssp0_write(&instr, 1);

    // 3 Bytes address
    addr_8 = (addr >> 16) & 0xFF;
    ssp0_write(&addr_8, 1);
    addr_8 = (addr >> 8) & 0xFF;
    ssp0_write(&addr_8, 1);
    addr_8 = addr & 0xFF;
    ssp0_write(&addr_8, 1);

    // 1 Page data
    ssp0_write(write_data, len);

    flash_cs_disable();
}

void flash_read(uint8_t *read_data, uint64_t addr, uint16_t len){
    uint8_t addr_8;

    flash_cs_enable();

    // 1 Byte read instruction
    instr = READ_DATA;
    ssp0_write(&instr, 1);

    // 3 Bytes address
    addr_8 = (addr >> 16) & 0xFF;
    ssp0_write(&addr_8, 1);
    addr_8 = (addr >> 8)  & 0xFF;
    ssp0_write(&addr_8, 1);
    addr_8 = addr & 0xFF;
    ssp0_write(&addr_8, 1);

    // date read
    ssp0_read(read_data, len);

    flash_cs_disable();
}

void flash_erase_4K(uint64_t addr){
    uint8_t addr_8;

    flash_cs_enable();

    // 1 Byte write enable
    instr = WRITE_ENABLE;
    ssp0_write(&instr, 1);

    flash_cs_disable();

    flash_cs_enable();

    // 1 Byte read instruction
    instr = SECTOR_ERASE_4K;
    ssp0_write(&instr, 1);

    // 3 Byte address
    addr_8 = (addr >> 16) & 0xFF;
    ssp0_write(&addr_8, 1);
    addr_8 = (addr >> 8)  & 0xFF;
    ssp0_write(&addr_8, 1);
    addr_8 = addr & 0xFF;
    ssp0_write(&addr_8, 1);

    flash_cs_disable();
}

void flash_chip_erase(void){
    flash_cs_enable();

    // 1 Byte write enable
    instr = WRITE_ENABLE;
    ssp0_write(&instr, 1);

    flash_cs_disable();
    for (uint32_t i = 0; i < 100000; i++)
        __asm("nop");

    flash_cs_enable();

    // 1 Byte erase instruction
    instr = CHIP_ERASE;
    ssp0_write(&instr, 1);

    flash_cs_disable();
}

uint8_t flash_status(uint8_t command){
    uint8_t read_status;

    flash_cs_enable();

    // 1 Byte instruction
    ssp0_write(&command, 1);

    // 2 Byte read status
    ssp0_read(&read_status, 1);

    flash_cs_disable();

    return read_status;
}

void flash_read_manu(uint8_t *manu_info){
    flash_cs_enable();

    // 1 Byte read instruction
    instr = READ_MANUFAC;
    ssp0_write(&instr, 1);

    // 3 Bytes address
    ssp0_write(0, 3);

    // date read
    ssp0_read(manu_info, 2);

    flash_cs_disable();
}

void flash_cs_pinset(void){
    // Set direction of P3.25 P3.26 to be output
    FIO0DIR2 |= 0b100000;
    FIO0SET2 |= 0b100000;
    FIO0DIR |= (1 << P0TMPCS);
    FIO0SET |= (1 << P0TMPCS);
}

void flash_cs_enable(void){
    // Set direction of P3.25 P3.26 pin output is set to HIGH
    // 0 : unchange
    // 1 : LOW
    FIO0CLR2 |= 0b100000;
    FIO0CLR |= (1 << P0TMPCS);
}

void flash_cs_disable(void){
    // Set direction of P3.25 P3.26 pin output is set to HIGH
    // 0 : unchange
    // 1 : HIGH
    FIO0SET2 |= 0b100000;
    FIO0SET |= (1 << P0TMPCS);
}