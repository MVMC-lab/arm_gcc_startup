/**
 * @file uart.c
 * @author LiYu87
 * @brief
 * @date 2020.02.17
 */

/**
 * @file uart.c
 * @author LiYu87
 * @date 2020.02.17
 * @brief uart3 operation functions
 */

#include "uart.h"
#include "regdef.h"

#include "LPC1769.h"

#define MUL_VAL 4
#define DIVADD_VAL 1

#define DLL_VAL 39
#define DLM_VAL 0

/**
 * pclk_uart3 = cpu_clk / 4
 *            = 120e6   / 4
 *            = 30e6
 *
 * baudrate = pclk / 16 / (256*DLM + DLL) / (1+ DIV/MUL)
 *          = 30e6 / 16 / (256* 0  +  39) / (1+  1 /  4)
 *          = 38461.5385
 *
 * err = (38461.5385-38400)/38400*100
 *     = 0.16%
 */
void uart3_init(void) {
    // enable uart fifo
    U3FCR = USART_FCR_FIFOEN;

    // get acess for Divisor Latch
    U3LCR = USART_LCR_DLAB;

    // set baudrate
    U3FDR = (MUL_VAL << USART_FDR_MULVAL_POS) |
                     (DIVADD_VAL << USART_FDR_DIVADDVAL_POS);
    U3DLL = DLL_VAL;
    U3DLM = DLM_VAL;

    // set character length as 8-bit
    // clr access for Divisor Latch
    U3LCR = (3 << USART_LCR_WLS_POS);
}

void uart3_putc(uint8_t data) {
    while (!(U3LSR & USART_LSR_THRE)) {
        ;
    }
    U3THR = data;
    while(!(U3LSR & USART_LSR_TEMT)) {
        ;
    }
}

uint8_t uart3_getc(void) {
    while (!(U3LSR & USART_LSR_RDR)) {
        ;
    }
    return U3RBR;
}
