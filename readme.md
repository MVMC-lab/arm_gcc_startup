# ARM GCC STARTUP

本專案旨在建立使用編譯器arm-gcc作為編譯器的環境。包含：

1. 暫存器 mapping
2. startup code
3. 專案組建範例

## 使用

1. clone 或 下載 此專案，到你本地專案資料夾中。
2. 更改 src 中的原始碼。
3. 更改 makefile 中相依的原始碼(SRC)。
4. make it!

## 檢查暫存器

目前暫存器的標頭檔(platform/LPC1769/LPC1769.h)是以人工方式做比較、以及更改，
仍有許多與 LPC1769 User Manual - UM10360 不同的地方，所以使用暫存器之前，需要檢查：

1. 暫存器 與 symbol 的名稱是否吻合
2. 暫存器的位址是否正確
3. 暫存器的型態是否正確，有uint32、uint8不同種類

使用者手冊可以到 NXP 官方下載： https://www.nxp.com/docs/en/user-guide/UM10360.pdf

若發現問題，請遵照下列步驟回報 issue。

## 暫存器問題回報

#### Step 1. 請開issue，並選擇 template `register-mapping`

到 [issue 頁面](https://gitlab.com/MVMC-lab/arm_gcc_startup/issues) 開啟新的 issue。

並在 template 欄位選擇 `register-mapping` 。

![image](/uploads/df6425a53e09e62c4284980e135f9230/image.png)

#### Step 2. 輸入使用的平台(IC)

在 Platform You Used 標題下輸入使用的 IC，像：

```
## Platform You Used

LPC1769
```

#### Step 3. 輸入你所比較的 User Manual

在 User Manual You Checked 標題下輸入你對照的文件，像：

```
## User Manual You Checked

UM10360
```

#### Step 4. 依照問題選擇 Problem Type

- register missing : 暫存器缺少，不在標頭檔中。
- register name different from User Manual : 暫存器名稱與使用手冊不同。
- register address wrong : 暫存器位址錯誤。
- else : 其他。

#### Step 5. 填入 User Manual 的頁數

填入你所參照 User Manual 的頁數，若方便請貼上圖片。

## 其他問題回報

若有遇到其他問題，請直接開啟新的issue撰寫即可，無需使用template。
