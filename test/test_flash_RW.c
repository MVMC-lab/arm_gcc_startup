/**
 * @file test_flash_RW.c
 * @author LCY
 * @brief FLASH 讀寫測試
 * @date 2020.04.30
 * 
 * 測試重點:
 *      1. erase 功能
 *      2. page program 功能
 *      3. status read 功能
 *      4. data read 功能
 * Note:
 *      讀寫占用時間十分重要，操作前請務必留意，以下為耗費時間表
 *      https://gitlab.com/MVMC-lab/arm_gcc_startup/-/wikis/FLASH
 *
 * FIXME: 使用邏輯分析儀一切皆正常，但以 print 印出資料會有不符的情況
 */

#include "uart.h"
#include "system.h"
#include "flash.h"
#include "LPC1769.h"
#include <stdio.h>

int main(void)
{
    setbuf(stdout, NULL);
    setbuf(stdin, NULL);

    system_clock_init();
    system_power_init();
    pinconnect_init();

    uart3_init();
    flash_init();

    printf("---START---\n");
    uint8_t status;

    // ********** I/O data initial **********
    uint64_t ADDR = 0x777777;
    uint8_t recv_data[10] = {0};

    uint8_t data[256] = {0};
    for (uint16_t i = 0; i < 256; i++)
        data[i] = 0x97;

    // ********** FLASH Operation **********
    // flash_chip_erase();
    
    flash_erase_4K(ADDR);
    // 50 ms
    for (uint32_t i = 0; i < 5000000; i++)
        __asm("nop");

    flash_write(data, ADDR , 10);
    // 5 ms
    for (uint32_t i = 0; i < 50000; i++)
        __asm("nop");

    do
    {
        status = flash_status(READ_STATUS_1);
    } while ((status & 0x11) != 0);

    status = flash_status(READ_STATUS_1);
    flash_read(recv_data, ADDR, 5);

    return 0;
}