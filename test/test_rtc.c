/**
 * @file test_rtc.c
 * @author LCY
 * @brief LPC1769 內部 rtc 功能測試
 * @date 2020.04.17
 */

#include "uart.h"
#include "system.h"
#include "LPC1769.h"
#include "rtc.h"
#include <stdio.h>

uint8_t ch;
float sd;
extern uint32_t __etext[];
extern uint32_t __data_start__[];

int main (void) {

    setbuf(stdout, NULL);
    setbuf(stdin, NULL);

    system_clock_init();
    system_power_init();
    pinconnect_init();

    uart3_init();

    printf("---START---\n");

    rtc_init(2020, 4, 17, 15, 0, 30);
    rtc_irq_en();
    rtc_start();
    __enable_irqn(RTC_IRQn);
    
    while (1)
    {
        ch = 0;
    }

    return 0;
}

void RTC_IRQHandler(void) {
    rtc_current_time();
    /* Clear interrupt flag */
    RTC_ILR |= 1;
}
